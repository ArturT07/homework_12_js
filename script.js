const buttons = document.querySelectorAll('.btn');
let activeButton = null;

buttons.forEach(button => {
    button.addEventListener('click', () => {
        toggleButtonColor(button);
    });
});

document.addEventListener('keydown', event => {
    if (event.code === 'Enter') {
        event.preventDefault();
        clearButtonColors();
    }
});

document.addEventListener('keyup', event => {
    const key = event.code.replace('Key', '');
    const targetButton = Array.from(buttons).find(button => button.textContent === key);

    if (targetButton) {
        clearButtonColors();
        toggleButtonColor(targetButton);
    }
});

function clearButtonColors() {
    buttons.forEach(button => {
        button.classList.remove('blue');
    });
}

function toggleButtonColor(button) {
    clearButtonColors();
    button.classList.add('blue');
    activeButton = button;
}








